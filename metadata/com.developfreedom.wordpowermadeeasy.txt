Categories:Reading
License:GPLv3
Web Site:http://developfreedom.com
Source Code:https://github.com/shubhamchaudhary/wordpowermadeeasy
Issue Tracker:https://github.com/shubhamchaudhary/wordpowermadeeasy/issues

Auto Name:Word Power Made Easy
Summary:Build your vocabualary
Description:
Vocabulary building with hundreds of word meanings chosen carefully from
various SAT, GRE and GMAT course materials.
.

Repo Type:git
Repo:https://github.com/shubhamchaudhary/wordpowermadeeasy.git

Build:0.1.0,1
    commit=v0.1.0
    subdir=app
    gradle=yes

Build:0.1.1,11
    commit=v0.1.1
    subdir=app
    gradle=yes

Build:0.1.2,12
    commit=v0.1.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.2
Current Version Code:12

